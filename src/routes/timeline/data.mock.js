import moment from 'moment'

const GROUPS = [
  {id: 1, title: 'group 1'},
  {id: 2, title: 'group 2'},
  {id: 3, title: 'group 3'},
  {id: 4, title: 'group 4'},
  {id: 5, title: 'group 5'},
  {id: 6, title: 'group 6'},
  {id: 7, title: 'group 7'},
  {id: 8, title: 'group 8'},
]

const ITEMS = [
  {id: 1, group: 1, title: 'item 1', start_time: moment(), end_time: moment().add(1, 'hour')},
  {id: 2, group: 2, title: 'item 2', start_time: moment().add(-0.5, 'hour'), end_time: moment().add(0.5, 'hour')},
  {id: 3, group: 3, title: 'item 3', start_time: moment().add(2, 'hour'), end_time: moment().add(3, 'hour')},
  {id: 4, group: 4, title: 'item 4', start_time: moment().add(3, 'hour'), end_time: moment().add(4, 'hour')},
  {id: 5, group: 5, title: 'item 5', start_time: moment().add(4, 'hour'), end_time: moment().add(5, 'hour')},
  {id: 6, group: 6, title: 'item 6', start_time: moment().add(5, 'hour'), end_time: moment().add(6, 'hour')},
  {id: 7, group: 7, title: 'item 7', start_time: moment().add(6, 'hour'), end_time: moment().add(7, 'hour')},
  {id: 8, group: 8, title: 'item 8', start_time: moment().add(7, 'hour'), end_time: moment().add(8, 'hour')},
]

export { GROUPS,ITEMS }
