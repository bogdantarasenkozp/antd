const USERS = [
  {
    name:'John Doe',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/10.jpg',
    time:'5 seconds ago'
  },
  {
    name:'Kerem Sure',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/9.jpg',
    time:'20 seconds ago'
  },
  {
    name:'Eric hoffman',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/8.jpg',
    time:'1 hour ago'
  },
  {
    name:'Eddie Lobanovskiy',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/7.jpg',
    time:'2 hour ago'
  },
  {
    name:'Bill S Kenney',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/6.jpg',
    time:'40 seconds ago'
  },
  {
    name:'Derek Bradley',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/5.jpg',
    time:'10 minutes ago'
  },
  {
    name:'Mariusz Ciesla',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/4.jpg',
    time:'5 hours ago'
  },
  {
    name:'Jesse Dodds',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/3.jpg',
    time:'12 seconds ago'
  },
  {
    name:'Gerren Lamson',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/2.jpg',
    time:'1 minute ago'
  },
  {
    name:'Daniel Waldron',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/1.jpg',
    time:'3 hours ago'
  },
  {
    name:'Celikovic',
    avatar:'https://www.b1g1.com/assets/admin/images/no_image_user.png',
    time:'25 seconds ago'
  },

  {
    name:'John Doe',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/10.jpg',
    time:'5 seconds ago'
  },
  {
    name:'Kerem Sure',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/9.jpg',
    time:'20 seconds ago'
  },
  {
    name:'Eric hoffman',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/8.jpg',
    time:'1 hour ago'
  },
  {
    name:'Eddie Lobanovskiy',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/7.jpg',
    time:'2 hour ago'
  },
  {
    name:'Bill S Kenney',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/6.jpg',
    time:'40 seconds ago'
  },
  {
    name:'Derek Bradley',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/5.jpg',
    time:'10 minutes ago'
  },
  {
    name:'Mariusz Ciesla',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/4.jpg',
    time:'5 hours ago'
  },
  {
    name:'Jesse Dodds',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/3.jpg',
    time:'12 seconds ago'
  },
  {
    name:'Gerren Lamson',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/2.jpg',
    time:'1 minute ago'
  },
  {
    name:'Daniel Waldron',
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/1.jpg',
    time:'3 hours ago'
  },
  {
    name:'Celikovic',
    avatar:'https://www.b1g1.com/assets/admin/images/no_image_user.png',
    time:'25 seconds ago'
  },
];

const MESSAGES = [
  {
    isAuthor:true,
    avatar:'https://www.b1g1.com/assets/admin/images/no_image_user.png',
    msgs:[
      {
        text: "Hello. What can I do for you?"
      }
    ]
  },
  {
    isAuthor:false,
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/1.jpg',
    msgs:[
      {
        text:"I'm just looking around.Will you tell me something about yourself?"
      },
      {
        text:"Are you there? That time!"
      }
    ]
  },
  {
    isAuthor:false,
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/1.jpg',
    msgs:[
      {
        text:"Lorem ipsum"
      }
    ]
  },
  {
    isAuthor:true,
    avatar:'https://www.b1g1.com/assets/admin/images/no_image_user.png',
    msgs:[
      {
        text: "Where?"
      },
      {
        text:"OK, my name is Limingqiang. I like singing, playing basketballand so on."
      },
      {
        text:"test"
      }
    ]
  },
  {
    isAuthor:false,
    avatar:'http://getbootstrapadmin.com/remark/global/portraits/1.jpg',
    msgs:[
      {
        text:"You wait for notice."
      }
    ]
  },
]

export { USERS, MESSAGES };
