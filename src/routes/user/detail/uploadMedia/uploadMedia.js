import React from 'react';
import { Upload, Button, Icon, Modal } from 'antd';
import { routerRedux } from 'dva/router'
import { connect } from 'dva'

export default class UploadMedia extends React.Component {

  constructor(props,context) {
    super(props,context)
    let files;
    let userId = location.pathname.replace(/\D/g,'');
    let filesList = JSON.parse(localStorage.getItem("files"));

    if (!filesList || filesList.length === 0) {
      files = []
    } else if (props.user.files.length === 0) {
      props.onInit(userId)
      files = filesList;
    } else {
      files = props.user.files;
    }

    files = files.filter((obj) => {
      if (obj.id === userId) {
        return obj;
      }
    });

    this.state = {
      userId: userId,
      previewVisible: false,
      previewImage: '',
      fileList: files,
    };
  }

  handleCancel = () => this.setState({ previewVisible: false });

  handleRemove = (file) => {
    localStorage.removeItem("files");
    this.props.onRemove(file.uid);
  }

  handleAdd = (file, fileType) => {
    let previewURL;
    let videoURL = 'video-placeholder.png';
    fileType === 'video' ? previewURL = videoURL : previewURL = file.response.url
    let data = {
      id: this.state.userId,
      uid: file.uid,
      name: previewURL,
      status: 'done',
      url: "http://localhost:3000/uploads/"+previewURL,
      src: file.response.url
    }
    this.props.onAdd(data);
  }

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true,
    });
  }

  handleChange = ({ file, fileList }) => {
    let fileType;
    if (file && file.status != "removed") {
      fileType = file.type.split('/')[0];
      if (file.response) {
          this.handleAdd(file, fileType)
      }
    }
    if (fileType  === 'video') {
      file.thumbUrl ="http://localhost:3000/uploads/video-placeholder.png"
    }
    this.setState({ fileList });
  }

  render() {
    const { previewVisible, previewImage, fileList } = this.state;
    const uploadProps = {
      action:    "http://localhost:3000/file/upload",
      listType:  "picture-card",
      fileList:  fileList,
      multiple:  true,
      onPreview: this.handlePreview,
      onChange:  this.handleChange,
      onRemove:  this.handleRemove,
    }
    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload File</div>
      </div>
    );
    return (
      <div className="clearfix">
        <Upload {...uploadProps}>
          {fileList.length >= 50 ? null : uploadButton}
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={previewImage} />
        </Modal>
      </div>
    );
  }
}
