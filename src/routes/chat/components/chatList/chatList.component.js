import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './chatList.component.css';
import { ChatSearch } from './../index';
const { Sider } = Layout;
export default class ChatList extends React.Component {
  constructor(props,context){
    super(props,context);
  }
  render () {
    return (
        <Menu
          mode="inline"
          style={{ overflowY: 'auto',overflowX: 'hidden' }}
          className={style.chatList}
        >
          {this.props.data.map((chat) => {
            return (
              <Menu.Item className={style.chatPreview}>
               <Badge dot>
                 <img className={style.chatAvatar} src={chat.avatar}/>
               </Badge>
               <div className={style.chatInfo}>
                 <div className={style.chatInfoTitle}>{chat.name}</div>
                 <div className={style.chatInfoTime}>{chat.time}</div>
               </div>
               <Badge count={25} />
             </Menu.Item>
            );
          })}
        </Menu>
    );
  }
}
