import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import UploadMedia from './uploadMedia/uploadMedia';
import styles from './index.less'

const Detail = ({ userDetail,dispatch }) => {
  const { data } = userDetail
  const content = []
  const filesProps = {
    onAdd (data) {
      dispatch({
        type: `userDetail/upload`,
        payload: data,
      })
    },
    onRemove (data) {
      dispatch({
        type: `userDetail/delete`,
        payload: data,
      })
    },
    onInit(data){
      dispatch({
        type: `userDetail/init`,
        payload: data,
      })
    },
  }
  for (let key in data) {
    if ({}.hasOwnProperty.call(data, key)) {
      content.push(<div key={key} className={styles.item}>
        <div>{key}</div>
        <div>{String(data[key])}</div>
      </div>)
    }
  }
  return (<div className="content-inner">
    <div className={styles.content}>
      {content}
    </div>
    <UploadMedia {...filesProps} user={userDetail}/>
  </div>)
}

Detail.propTypes = {
  userDetail: PropTypes.object,
  loading: PropTypes.bool,
}

export default connect(({ userDetail, loading }) => ({ userDetail, loading: loading.models.userDetail }))(Detail)
