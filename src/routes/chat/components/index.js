import ChatHeader  from './chatHeader/chatHeader.component';
import ChatList    from './chatList/chatList.component';
import HistoryBtn  from './historyBtn/historyBtn.component';
import HistoryInfo from './historyInfo/historyInfo.component';
import ChatSearch  from './chatSearch/chatSearch.component';
import ChatMsg     from './chatMsg/chatMsg.component';
import ChatInput   from './chatInput/chatInput.component';
import Aside       from './aside/aside.component';
export { ChatHeader, ChatList,ChatSearch, HistoryBtn, HistoryInfo, ChatMsg, ChatInput, Aside };
