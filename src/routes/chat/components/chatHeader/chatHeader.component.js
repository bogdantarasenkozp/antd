import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './chatHeader.component.css';
const { Header } = Layout;
export default class ChatHeader extends React.Component {
  constructor (props,context) {
    super(props,context)
    this.state = { hide:false };
    this.toggleAsideClick = this.toggleAsideClick.bind(this);
  }
  toggleAsideClick () {
    this.setState({ hide: !this.state.hide});
  }
  render () {
    return (
      <Header className={style.header}>
        <Menu
          theme="light"
          mode="horizontal"
          defaultSelectedKeys={['2']}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item className={style.navItem} key="1"><Icon type="arrow-left" /></Menu.Item>
          <Menu.Item className={style.navItem} key="2"><Icon type="search" /></Menu.Item>
          <Menu.Item className={style.navItem} key="3"><Icon type="arrows-alt" /></Menu.Item>
          <Menu.Item className={style.navItem} key="4">
            { this.state.hide === false ?
              ( <div className={style.asideHide} onClick={this.toggleAsideClick.bind(this)}><Icon type="up" /></div> ) :
              ( <div className={style.asideShow} onClick={this.toggleAsideClick.bind(this)}><Icon type="down" /></div> )
            }
          </Menu.Item>
          { this.state.hide === false &&
            <Menu.Item className={style.rightItem} key="6">
              <Badge dot>
                <Icon type="notification" />
              </Badge>
            </Menu.Item>
          }
          { this.state.hide === false &&
            <Menu.Item className={style.rightItem} key="7">
              <Badge dot>
                <Icon type="mail" />
              </Badge>
            </Menu.Item>
          }
          { this.state.hide === false &&
            <Menu.Item className={style.rightItem} key="8">
              <Badge dot>
                <Icon type="message" />
              </Badge>
            </Menu.Item>
          }
          { this.state.hide === false &&
            <Menu.Item className={style.rightItem} key="5">
              <Badge dot>
                <img className={style.userAvatar}
                     src="https://www.b1g1.com/assets/admin/images/no_image_user.png"
                />
              </Badge>
            </Menu.Item>
          }
        </Menu>
      </Header>
    );
  }

}
