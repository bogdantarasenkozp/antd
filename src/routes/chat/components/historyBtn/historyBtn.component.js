import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './historyBtn.component.css';

export default class HistoryBtn extends React.Component {

  render () {
    return (
      <Row gutter={12} className={style.historyBtn}>
        <Button >History Messages</Button>
      </Row>
    );
  }
}
