import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style     from './chatMsg.component.css';
import LeftChat  from './leftChat.component';
import RightChat from './rightChat.component';
export default class ChatMsg extends React.Component {
  constructor(props,context){
    super(props,context);
  }

  render () {
    return (
      <div>
      {this.props.data.map((chat)=>{
        return (
          <div>
            { chat.isAuthor === false ?
              ( <LeftChat data={chat} /> ) :
              ( <RightChat data={chat} />)
            }
          </div>
        )
      })}
    </div>
    )
  }

}
