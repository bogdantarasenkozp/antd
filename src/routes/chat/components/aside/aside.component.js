import React from 'react'
import { Icon } from 'antd';
import style from './aside.component.css';

export default class Aside extends React.Component {
  constructor (props,context) {
    super(props,context)
    this.state = { show:false };
    this.toggleAsideClick = this.toggleAsideClick.bind(this);
  }
  toggleAsideClick () {
    this.setState({ show: !this.state.show});
  }
  render () {
    return (
      <div>
        { this.state.show === false ?
          ( <div className={style.asideHide} onClick={this.toggleAsideClick.bind(this)}><Icon type="left" /></div> ) :
          ( <div className={style.asideShow} onClick={this.toggleAsideClick.bind(this)}><Icon type="right" /></div> )
        }
      </div>
    )
  }
}
