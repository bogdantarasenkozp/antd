import { request, config } from '../utils'
import axios from 'axios';
const { api } = config
const { user } = api

export async function query (params) {
  return request({
    url: user,
    method: 'get',
    data: params,
  })
}

export async function create (params) {
  return request({
    url: user.replace('/:id', ''),
    method: 'post',
    data: params,
  })
}

export async function remove (params) {
  return request({
    url: user,
    method: 'delete',
    data: params,
  })
}

export async function update (params) {
  return request({
    url: user,
    method: 'patch',
    data: params,
  })
}

export async function deleteMedia (params) {
  axios({
    method: 'delete',
    url: 'http://localhost:3000/file/delete/',
    data: {
      path: params,
    }
  });
}
