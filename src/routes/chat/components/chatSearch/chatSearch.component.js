import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './chatSearch.component.css';
const Search = Input.Search;

export default class ChatSearch extends React.Component {
  render () {
    return (
      <div className={style.chatListSearch}>
        <Search className={style.searchForm} placeholder="Basic usage" />
      </div>
    )
  }
}
