import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './chatMsg.component.css';

const RightChat = (props) => {
  return (
    <div className={style.chatMsgs}>
      <Row gutter={12} type="flex" className={style.rightChat}>
        <Col className={style.chatUserAvatar}>
          <a>
           <img src={props.data.avatar}/>
          </a>
        </Col>
        <Col type="flex" className={style.chatBody}>
          {props.data.msgs.map((msg)=>{
            return (
              <Col className={style.chatContent}>
                {msg.text}
              </Col>
            );
          })}
        </Col>
      </Row>
    </div>
  );
}

export default RightChat;
