import React from 'react'
import { Layout,Col, Icon } from 'antd';
import { ChatHeader, ChatList, ChatMsg, ChatInput, ChatSearch, HistoryBtn, HistoryInfo, Aside } from './components/';
import { USERS,MESSAGES } from './data.mock.js';
import style from './chat.css';
const { Header, Content, Sider } = Layout;

export default class EditorPage extends React.Component {
  constructor (props,context) {
    super(props,context)
    let hideSidebar;
    if(screen.width >= 1024) {
      hideSidebar = false;
    } else {
      hideSidebar = true;
    }
    this.state = { hide:hideSidebar };
    this.toggleAsideClick = this.toggleAsideClick.bind(this);
  }
  toggleAsideClick () {
    this.setState({ hide: !this.state.hide});
  }
  render () {
    return (
      <Layout>
        <ChatHeader />
        <Content style={{ padding: '0 0px' }}>
          <Layout style={{ padding: '0px 0',height: '100vh', background: '#fff' }}>
            <div>
              { this.state.hide === false ?
                ( <div className={style.asideHide} onClick={this.toggleAsideClick.bind(this)}><Icon type="left" /></div> ) :
                ( <div className={style.asideShow} onClick={this.toggleAsideClick.bind(this)}><Icon type="right" /></div> )
              }
            </div>
            { this.state.hide === false &&
              <Sider width={250} className={style.sidebar} style={{ background: '#fff', overflow: 'auto' }}>
                <ChatSearch />
                <ChatList data={USERS}/>
              </Sider>
            }
            <Content style={{ minHeight: 280,overflow:'hidden' }}>
              <div className={style.chatContainer}>
                <HistoryBtn />
                <HistoryInfo />
                <ChatMsg data={MESSAGES} />
              </div>
              <ChatInput />
            </Content>
          </Layout>
        </Content>
      </Layout>
    )
  }
}
