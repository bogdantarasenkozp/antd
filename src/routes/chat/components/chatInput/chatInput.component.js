import React from 'react';
import { Row, Col, Card, Input, Form, Layout, Menu, Breadcrumb, Icon, Badge, Button } from 'antd';
import style from './chatInput.component.css';

export default class ChatInput extends React.Component {

  render () {
    return (
      <Form className={style.messageInputForm}>
        <div className={style.messageInput}>
          <Input placeholder="Basic usage"
            className={style.formControl}
            style={{overflow: 'hidden', wordWrap: 'break-word', height: '45px',width: '98%'}}
            suffix={<Row className={style.inputIcons}>
              <Icon type="smile" />
              <Icon type="link" />
              <Icon type="picture" />
              </Row>}
          />
        </div>
        <Button className={style.messageInputBtn} type="primary">send</Button>
      </Form>
    );
  }

}
