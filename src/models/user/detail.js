import pathToRegexp from 'path-to-regexp'
import { query, deleteMedia } from '../../services/user'

export default {

  namespace: 'userDetail',

  state: {
    data: {},
    files: []
  },

  subscriptions: {
    setup ({ dispatch, history }) {
      history.listen(() => {
        const match = pathToRegexp('/user/:id').exec(location.pathname)
        if (match) {
          dispatch({ type: 'query', payload: { id: match[1] } })
        }
      })
    },
  },

  effects: {
    *query ({
      payload,
    }, { call, put }) {
      const data = yield call(query, payload)
      const { success, message, status, ...other } = data
      if (success) {
        yield put({
          type: 'querySuccess',
          payload: {
            data: other,
          },
        })
      } else {
        throw data
      }
    },
  },

  reducers: {
    querySuccess (state, { payload }) {
      const { data } = payload
      return {
        ...state,
        data,
      }
    },
    upload (state, { payload }) {
      let files = [...state.files, payload];
      localStorage.removeItem("files");
      localStorage.setItem("files", JSON.stringify(files));
      return {
        ...state,
        files,
      }
    },
    init (state, { payload }) {
      let files = JSON.parse(localStorage.getItem("files"));
      if (!files) {
        files = [];
      }
      return {
        ...state,
        files,
      }
    },
    delete (state, { payload }) {
      let files = state.files;
      let index = files.findIndex(obj => obj.uid === payload)
      deleteMedia(files[index].src);
      files.splice(index, 1);
      localStorage.removeItem("files");
      localStorage.setItem("files", JSON.stringify(files));

      return {
        ...state,
        files,
      }
    }
  },
}
