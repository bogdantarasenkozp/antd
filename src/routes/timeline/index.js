import React from 'react';
import Timeline from 'react-calendar-timeline'
import moment from 'moment'
import { GROUPS,ITEMS } from './data.mock'

export default class CalendarTimeline extends React.Component {
  render () {
    return (
      <div>
        <Timeline groups={GROUPS}
                  items={ITEMS}
                  defaultTimeStart={moment().add(-12, 'hour')}
                  defaultTimeEnd={moment().add(12, 'hour')}
                  />
      </div>
    );
  }
}
